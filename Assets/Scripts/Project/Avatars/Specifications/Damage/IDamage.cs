namespace Project.Avatars.Specifications.Damage
{
    public interface IDamage
    {
        public float GetMax();
        public float GetMin();
        public Effect GetEffect();
    }
}
