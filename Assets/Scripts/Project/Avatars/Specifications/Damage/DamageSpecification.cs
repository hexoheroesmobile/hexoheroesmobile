using Common.Utils;
using Project.Avatars.Specifications.Health.Effects;

namespace Project.Avatars.Specifications.Damage
{
    public class DamageSpecification : Specification, IDamage
    {
        private float _min;
        private float _max;

        public DamageSpecification(float min, float max)
        {
            _min = min;
            _max = max;
        }
        
        public float GetMin()
        {
            return _min;
        }

        public float GetMax()
        {
            return _max;
        }

        public Effect GetEffect()
        {
            var value = new RandomUtils().RandomBetween(_min, _max);
            return new MeleeDamageEffect(value);
        }
    }
}
