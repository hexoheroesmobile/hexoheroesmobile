using UnityEngine;
using UnityEngine.Events;

namespace Project.Avatars.Specifications.Initiative
{
    public class InitiativeSpecification : Specification, IInitiative
    {
        public UnityEvent Changed { get; } = new UnityEvent();
        
        private float _max;
        private float _current;

        public InitiativeSpecification(float max, float current)
        {
            _max = max;
            _current = current;
        }

        public float GetMax()
        {
            return _max;
        }

        public float GetCurrent()
        {
            return _current;
        }

        public void AddMax(float value)
        {
            _max += Mathf.Abs(value);
            Changed.Invoke();
        }

        public void SubMax(float value)
        {
            value = Mathf.Abs(value);
            _max = Mathf.Max(0.0f, _max - value);
            Changed.Invoke();
        }

        public void AddCurrent(float value)
        {
            value = Mathf.Abs(value);
            _current = Mathf.Clamp(_current + value, 0.0f, _max);
            Changed.Invoke();
        }

        public void SubCurrent(float value)
        {
            value = Mathf.Abs(value);
            _current = Mathf.Clamp(_current - value, 0.0f, _max);
            Changed.Invoke();
        }
    }
}