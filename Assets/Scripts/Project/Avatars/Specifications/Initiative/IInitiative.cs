namespace Project.Avatars.Specifications.Initiative
{
    public interface IInitiative
    {
        public float GetMax();
        public float GetCurrent();
        public void AddMax(float value);
        public void SubMax(float value);
        public void AddCurrent(float value);
        public void SubCurrent(float value);
    }
}