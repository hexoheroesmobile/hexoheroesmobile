using UnityEngine;
using UnityEngine.Events;

namespace Project.Avatars.Specifications.Health
{
    public class HealthSpecification : Specification, IHealth
    {
        public UnityEvent Over { get; } = new UnityEvent();
        
        private float _max;
        private float _current;

        public HealthSpecification(float max, float current)
        {
            _max = max;
            _current = current;
        }

        public float GetMax()
        {
            return _max;
        }

        public float GetCurrent()
        {
            return _current;
        }

        public void AddMax(float value)
        {
            _max += Mathf.Abs(value);
        }

        public void SubMax(float value)
        {
            value = Mathf.Abs(value);
            _max = Mathf.Max(0.0f, _max - value);
        }

        public void AddCurrent(float value)
        {
            value = Mathf.Abs(value);
            _current = Mathf.Clamp(_current + value, 0.0f, _max);
        }

        public void SubCurrent(float value)
        {
            value = Mathf.Abs(value);
            _current = Mathf.Clamp(_current - value, 0.0f, _max);
            if (_current == 0.0f)
            {
                Over.Invoke();
            }
        }
    }
}
