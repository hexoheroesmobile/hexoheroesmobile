namespace Project.Avatars.Specifications.Health
{
    public interface IHealth
    {
        public float GetMax();
        public float GetCurrent();
        public void AddMax(float value);
        public void SubMax(float value);
        public void AddCurrent(float value);
        public void SubCurrent(float value);
    }
}
