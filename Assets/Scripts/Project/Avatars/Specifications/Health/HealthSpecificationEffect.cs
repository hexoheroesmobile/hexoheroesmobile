using UnityEngine;
using UnityEngine.Events;

namespace Project.Avatars.Specifications.Health
{
    public class HealthSpecificationEffect : Effect, IHealth
    {
        protected float Max;
        protected float Current;
        
        public float GetMax()
        {
            return Max;
        }

        public float GetCurrent()
        {
            return Current;
        }

        public virtual void AddMax(float value)
        {
            Max += Mathf.Abs(value);
        }

        public virtual void SubMax(float value)
        {
            value = Mathf.Abs(value);
            Max = Mathf.Max(0.0f, Max - value);
        }

        public virtual void AddCurrent(float value)
        {
            value = Mathf.Abs(value);
            Current = Mathf.Clamp(Current + value, 0.0f, Max);
        }

        public virtual void SubCurrent(float value)
        {
            value = Mathf.Abs(value);
            Current = Mathf.Clamp(Current - value, 0.0f, Max);
        }
        
        public override void Apply(Avatar avatar)
        {
            Avatar = avatar;
            Max = avatar.AvatarData.Specifications.GetHealth().GetMax();
            Current = avatar.AvatarData.Specifications.GetHealth().GetCurrent();
        }
        
        public override void Remove()
        {
            Avatar = null;
            Max = default;
            Current = default;
        }
    }
}
