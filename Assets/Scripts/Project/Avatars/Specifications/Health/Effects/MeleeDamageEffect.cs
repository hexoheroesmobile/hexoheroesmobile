namespace Project.Avatars.Specifications.Health.Effects
{
    public class MeleeDamageEffect : HealthSpecificationOrderedEffect
    {
        private readonly float _value;
        
        public MeleeDamageEffect(float value)
        {
            _value = value;
            Order = 999;
        }
        
        protected override void AfterApply()
        {
            SubCurrent(_value);
            Remove();
        }
    }
}
