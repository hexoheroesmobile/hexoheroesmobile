using UnityEngine;

namespace Project.Avatars.Specifications.Health
{
    public abstract class HealthSpecificationOrderedEffect : HealthSpecificationEffect
    {
        private IHealth Parent { get; set; }
        public IHealth Child { get; private set; }
        public int Order { get; protected set; }

        public sealed override void Apply(Avatar avatar)
        {
            BeforeApply();

            Avatar = avatar;

            var iterator = new HealthSpecificationOrderedEffectsIterator(avatar.AvatarData.Specifications.GetHealth());
            iterator.FindEffectPosition(Order);
            Parent = iterator.GetParent();
            Child = iterator.GetCurrent();

            if (IsEffect(Child))
            {
                var effect = GetEffect(Child);
                if (effect.Order == Order)
                {
                    effect.Consume(this);
                    effect.Recalculate();
                    return;
                }
            }

            if (Parent == null)
            {
                avatar.AvatarData.Specifications.SetHealth(this);
            }
            else
            {
                var parentEffect = GetEffect(Parent);
                parentEffect.Child = this;
                if (IsEffect(Child))
                {
                    GetEffect(Child).Parent = this;
                }
            }

            Recalculate();

            AfterApply();
        }

        public sealed override void Remove()
        {
            BeforeRemove();

            if (Parent == null)
            {
                Avatar.AvatarData.Specifications.SetHealth(Child);
            }
            else
            {
                GetEffect(Parent).Child = Child;
            }

            if (IsEffect(Child))
            {
                GetEffect(Child).Parent = Parent;
            }

            Avatar = null;
            Max = default;
            Current = default;
            Parent = null;
            Child = null;

            AfterRemove();
        }

        public sealed override void AddMax(float value)
        {
            Max += Mathf.Abs(value);
            Child.AddMax(CalculateOperationsEffect(value));
        }

        public sealed override void SubMax(float value)
        {
            value = Mathf.Abs(value);
            Max = Mathf.Max(0.0f, Max - value);
            Child.SubMax(CalculateOperationsEffect(value));
        }

        public sealed override void AddCurrent(float value)
        {
            Current += Mathf.Abs(value);
            Child.AddCurrent(CalculateOperationsEffect(value));
        }

        public sealed override void SubCurrent(float value)
        {
            value = Mathf.Abs(value);
            Current = Mathf.Clamp(Current - value, 0.0f, Max);
            Child.SubCurrent(CalculateOperationsEffect(value));
        }

        protected virtual void BeforeApply()
        {

        }

        protected virtual void AfterApply()
        {

        }

        protected virtual void BeforeRemove()
        {

        }

        protected virtual void AfterRemove()
        {

        }

        protected virtual void Consume(HealthSpecificationOrderedEffect targetEffect)
        {

        }

        protected virtual float CalculateOperationsEffect(float initValue)
        {
            return initValue;
        }

        protected virtual float CalculateStatEffect(float initValue)
        {
            return initValue;
        }

        private void Recalculate()
        {
            Max = CalculateStatEffect(Child.GetMax());
            Current = CalculateStatEffect(Child.GetCurrent());

            if (Parent != null)
            {
                GetEffect(Parent).Recalculate();
            }
        }

        private HealthSpecificationOrderedEffect GetEffect(IHealth effect)
        {
            return effect as HealthSpecificationOrderedEffect;
        }

        private bool IsEffect(IHealth health)
        {
            return GetEffect(health) != null;
        }
    }
}
