namespace Project.Avatars.Specifications.Health
{
    public class HealthSpecificationOrderedEffectsIterator
    {
        private IHealth _prev;
        private IHealth _current;

        public HealthSpecificationOrderedEffectsIterator(IHealth start)
        {
            _prev = null;
            _current = start;
        }

        public void FindEffectPosition(int order)
        {
            if (IsEffect(_current) == false) return;
            while (GetEffectOrder(_current) > order)
            {
                if (MoveNext() == false) break;
            }
        }

        public bool MoveNext()
        {
            if (IsEffect(_current) == false) return false;
            _prev = _current;
            _current = GetEffect(_current).Child;
            return true;
        }
        
        public IHealth GetParent()
        {
            return _prev;
        }

        public IHealth GetCurrent()
        {
            return _current;
        }

        private bool IsEffect(IHealth health)
        {
            return GetEffect(health) != null;
        }
        
        private HealthSpecificationOrderedEffect GetEffect(IHealth effect)
        {
            return effect as HealthSpecificationOrderedEffect;
        }
        
        private int GetEffectOrder(IHealth effect)
        {
            return IsEffect(effect) == false ? 0 : GetEffect(effect).Order;
        }
    }
}
