namespace Project.Avatars.Specifications
{
    public abstract class Effect
    {
        protected Avatar Avatar { get; set; }

        public virtual void Apply(Avatar avatar)
        {
            Avatar = avatar;
        }

        public virtual void Remove()
        {
            Avatar = null;
        }
    }
}
