using Common.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace Project.Avatars.Specifications.MovesCount
{
    public class MovesCountSpecification : Specification, IMovesCount
    {
        public UnityEvent Changed { get; } = new UnityEvent();
        
        private int _max;
        private int _current;

        public MovesCountSpecification(int max, int current)
        {
            _max = max;
            _current = current;
        }

        public int GetMax()
        {
            return _max;
        }

        public int GetCurrent()
        {
            return _current;
        }

        public void AddMax(int value)
        {
            _max += Mathf.Abs(value);
            Changed.Invoke();
        }

        public void SubMax(int value)
        {
            value = Mathf.Abs(value);
            _max = Mathf.Max(0, _max - value);
            Changed.Invoke();
        }

        public void AddCurrent(int value)
        {
            value = Mathf.Abs(value);
            _current = Mathf.Clamp(_current + value, 0, int.MaxValue);
            Changed.Invoke();
        }

        public void SubCurrent(int value)
        {
            value = Mathf.Abs(value);
            _current = Mathf.Clamp(_current - value, 0, int.MaxValue);
            Changed.Invoke();
        }
    }
}
