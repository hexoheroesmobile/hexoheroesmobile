namespace Project.Avatars.Specifications.MovesCount
{
    public interface IMovesCount
    {
        public int GetMax();
        public int GetCurrent();
        public void AddMax(int value);
        public void SubMax(int value);
        public void AddCurrent(int value);
        public void SubCurrent(int value);
    }
}
