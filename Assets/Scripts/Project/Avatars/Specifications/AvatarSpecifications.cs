using System;
using Project.Avatars.Specifications.Damage;
using Project.Avatars.Specifications.Health;
using Project.Avatars.Specifications.Initiative;
using Project.Avatars.Specifications.MovesCount;
using UnityEngine.Events;

namespace Project.Avatars.Specifications
{
    public class AvatarSpecifications
    {
        public UnityEvent HealthIsOver { get; } = new UnityEvent();
        public UnityEvent InitiativeIsChanged { get; } = new UnityEvent();
        public UnityEvent MovesCountIsChanged { get; } = new UnityEvent();
        
        private IHealth _health;
        private IDamage _damage;
        private IInitiative _initiative;
        private IMovesCount _movesCount;

        public void SetHealth(IHealth health)
        {
            if (_health == null)
            {
                // if == null then parameter is a specification
                ((HealthSpecification)health).Over.AddListener(OnHealthIsOver);
            }
            _health = health;
        }

        public IHealth GetHealth()
        {
            return _health;
        }

        public void SetDamage(IDamage damage)
        {
            _damage = damage;
        }

        public IDamage GetDamage()
        {
            return _damage;
        }
        
        public void SetInitiative(IInitiative initiative)
        {
            if (_initiative == null)
            {
                // if == null then parameter is a specification
                ((InitiativeSpecification)initiative).Changed.AddListener(OnInitiativeIsChanged);
            }
            _initiative = initiative;
        }

        public IInitiative GetInitiative()
        {
            return _initiative;
        }
        
        public void SetMovesCount(IMovesCount movesCount)
        {
            if (_movesCount == null)
            {
                // if == null then parameter is a specification
                ((MovesCountSpecification)movesCount).Changed.AddListener(OnMovesCountIsChanged);
            }
            _movesCount = movesCount;
        }

        public IMovesCount GetMovesCount()
        {
            return _movesCount;
        }

        public void OnInitiativeIsChanged()
        {
            InitiativeIsChanged.Invoke();
        }
        
        private void OnHealthIsOver()
        {
            HealthIsOver.Invoke();
        }
        
        private void OnMovesCountIsChanged()
        {
            MovesCountIsChanged.Invoke();
        }
    }
}
