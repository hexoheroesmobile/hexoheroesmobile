using Common.Utils;
using Project.Avatars.Specifications;
using Project.Avatars.Specifications.Damage;
using Project.Avatars.Specifications.Health;
using Project.Avatars.Specifications.Initiative;
using Project.Avatars.Specifications.MovesCount;
using UnityEngine;

namespace Project.Avatars.Enemies
{
    public class EnemyBattleAvatarTemplate : MonoBehaviour
    {
        [SerializeField] private int _power;
        [SerializeField] private EnemyBattleAvatar _avatar;

        [SerializeField] private float _initiative;
        [SerializeField] private float _health;
        [SerializeField] private float _minDamage;
        [SerializeField] private float _maxDamage;
        [SerializeField] private int _movesCount;
        
        public int GetPower()
        {
            return _power;
        }

        public EnemyBattleAvatar Create()
        {
            var specifications = new AvatarSpecifications();
            
            // TODO Add logic
            specifications.SetInitiative(new InitiativeSpecification(_initiative, _initiative));
            specifications.SetHealth(new HealthSpecification(_health, _health));
            specifications.SetDamage(new DamageSpecification(_minDamage, _maxDamage));
            specifications.SetMovesCount(new MovesCountSpecification(_movesCount, _movesCount));
            
            var enemyHubAvatar = GameObjectUtils.InstantiateNotActive(_avatar, null, true);
            var avatarData = new AvatarData();
            avatarData.SetSpecifications(specifications);
            enemyHubAvatar.SetAvatarData(avatarData);

            return enemyHubAvatar;
        }
    }
}
