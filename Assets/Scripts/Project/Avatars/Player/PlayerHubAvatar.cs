using UnityEngine.Events;

namespace Project.Avatars.Player
{
    public class PlayerHubAvatar : Avatar
    {
        public UnityEvent Selected { get; } = new UnityEvent();

        private void OnMouseDown()
        {
            Selected.Invoke();
        }
    }
}
