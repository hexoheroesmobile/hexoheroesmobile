using Project.Avatars.Specifications;
using UnityEngine;

namespace Project.Avatars
{
    public class Avatar : MonoBehaviour
    {
        public AvatarData AvatarData { get; private set; }
        
        public void SetAvatarData(AvatarData data)
        {
            AvatarData ??= data;
        }
        
        public void ApplyEffect(Effect effect)
        {
            effect.Apply(this);
        }
    }
}
