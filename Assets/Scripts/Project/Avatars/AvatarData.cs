using System.Collections.Generic;
using Project.Avatars.Artifacts;
using Project.Avatars.Specifications;

namespace Project.Avatars
{
    public class AvatarData
    {
        public List<Artifact> Artifacts { get; private set; }
        public AvatarSpecifications Specifications { get; private set; }

        public void SetArtifacts(List<Artifact> artifacts)
        {
            Artifacts ??= artifacts;
        }
        
        public void SetSpecifications(AvatarSpecifications specifications)
        {
            Specifications ??= specifications;
        }
    }
}
