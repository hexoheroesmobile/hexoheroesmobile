using System.Collections.Generic;
using System.Linq;
using Project.Avatars.Player;
using UnityEngine;
using UnityEngine.Events;
using Avatar = Project.Avatars.Avatar;

namespace Project.Scenes.Battle.Queue
{
    public class BattleQueue : MonoBehaviour
    {
        public UnityEvent Changed { get; } = new UnityEvent();
        
        private List<BattleQueueElement> _queue;

        private List<Avatar> _playerAvatars;
        private List<Avatar> _enemyAvatars;

        public void Init(List<Avatar> playerAvatars, List<Avatar> enemyAvatars)
        {
            foreach (var playerAvatar in playerAvatars)
            {
                playerAvatar.AvatarData.Specifications.HealthIsOver.AddListener(() => AvatarHealthIsOver(playerAvatar));
                playerAvatar.AvatarData.Specifications.InitiativeIsChanged.AddListener(() => AvatarInitiativeIsChanged(playerAvatar));
            }
            
            foreach (var enemyAvatar in enemyAvatars)
            {
                enemyAvatar.AvatarData.Specifications.HealthIsOver.AddListener(() => AvatarHealthIsOver(enemyAvatar));
                enemyAvatar.AvatarData.Specifications.InitiativeIsChanged.AddListener(() => AvatarInitiativeIsChanged(enemyAvatar));
            }

            _playerAvatars = playerAvatars;
            _enemyAvatars = enemyAvatars;

            CalculateQueue();
        }

        public Avatar GetCurrentAvatar()
        {
            return _queue[0].Avatar;
        }

        public void NextTurn()
        {
            CalculateQueue();
        }

        private void CalculateQueue()
        {
            var allAvatars = new List<Avatar>();
            allAvatars.AddRange(_playerAvatars);
            allAvatars.AddRange(_enemyAvatars);
            
            allAvatars.RemoveAll(avatar => avatar.AvatarData.Specifications.GetMovesCount().GetCurrent() == 0);
            allAvatars = allAvatars.OrderByDescending(avatar => avatar.AvatarData.Specifications.GetInitiative().GetCurrent()).ToList();
            // other conditions

            _queue = new List<BattleQueueElement>();
            foreach (var avatar in allAvatars)
            {
                var queueElement = new BattleQueueElement
                {
                    Avatar = avatar,
                    Side = GetSide(avatar)
                };
                _queue.Add(queueElement);
            }
            
            Changed.Invoke();
        }

        private void AvatarHealthIsOver(Avatar avatar)
        {
            CalculateQueue();
        }

        private void AvatarInitiativeIsChanged(Avatar avatar)
        {
            CalculateQueue();
        }

        private BattleSides GetSide(Avatar avatar)
        {
            if (avatar is PlayerBattleAvatar)
            {
                return BattleSides.Player;
            }
            else
            {
                return BattleSides.Enemy;
            }
        }
    }
}
