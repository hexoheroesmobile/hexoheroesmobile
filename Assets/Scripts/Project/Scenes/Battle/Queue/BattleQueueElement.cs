using UnityEngine;
using Avatar = Project.Avatars.Avatar;

namespace Project.Scenes.Battle.Queue
{
    public class BattleQueueElement
    {
        public BattleSides Side { get; set; }
        public Avatar Avatar { get; set; }
    }
}
