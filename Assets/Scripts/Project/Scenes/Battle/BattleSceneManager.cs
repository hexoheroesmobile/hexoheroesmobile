using System.Linq;
using Common.Scenes;
using Project.Scenes.Battle.Queue;
using Project.Scenes.Transfers;
using UnityEditor;
using UnityEngine;
using Avatar = Project.Avatars.Avatar;

namespace Project.Scenes.Battle
{
    public class BattleSceneManager : ProjectSceneManager
    {
        [SerializeField] private SceneAsset _afterBattleScene;
        [SerializeField] private BattlePlayerAvatarsManager _battlePlayerAvatarsManager;
        [SerializeField] private BattleEnemyAvatarsManager _battleEnemyAvatarsManager;
        [SerializeField] private BattleManager _battleManager;
        [SerializeField] private BattleQueue _battleQueue;
        [SerializeField] private BattleToAfterBattlePreloaderTransfer _battleToAfterBattlePreloaderTransfer;
        
        private void Start()
        {
            var prevSceneTransfer = FindObjectOfType<BattlePreloaderToBattleTransfer>();
            var playerBattleAvatars = prevSceneTransfer.PlayerBattleAvatars;
            var enemyBattleAvatars = prevSceneTransfer.EnemyBattleAvatars;

            var playerAvatars = playerBattleAvatars.Cast<Avatar>().ToList();
            var enemyAvatars = enemyBattleAvatars.Cast<Avatar>().ToList();
            _battleQueue.Init(playerAvatars, enemyAvatars);
            
            _battlePlayerAvatarsManager.SetAvatars(playerBattleAvatars);
            _battleEnemyAvatarsManager.SetAvatars(enemyBattleAvatars);
            _battleManager.StartBattle();
            
            IsFullyLoaded = true;
        }

        public void Finish()
        {
            StartCoroutine(LoadNextSceneWithBeforeStep(PrepareAvatars, _afterBattleScene));
        }
        
        private void PrepareAvatars()
        {
            _battlePlayerAvatarsManager.UnloadAvatars();
            _battleToAfterBattlePreloaderTransfer.Avatars = _battlePlayerAvatarsManager.Avatars;
        }
    }
}