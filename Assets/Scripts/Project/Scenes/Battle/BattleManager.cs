using System.Collections.Generic;
using System.Linq;
using Project.Scenes.Battle.Queue;
using UnityEngine;

namespace Project.Scenes.Battle
{
    public class BattleManager : MonoBehaviour
    {
        [SerializeField] private BattleSceneManager _battleSceneManager;
        [SerializeField] private BattlePlayerAvatarsManager _battlePlayerAvatarsManager;
        [SerializeField] private BattleEnemyAvatarsManager _battleEnemyAvatarsManager;
        [SerializeField] private BattleQueue _battleQueue;

        public void StartBattle()
        {
            
        }

        private void NextRound()
        {
            
        }

        private void NextTurn()
        {
            
        }

        private void FinishBattle()
        {
            
        }
    }
}
