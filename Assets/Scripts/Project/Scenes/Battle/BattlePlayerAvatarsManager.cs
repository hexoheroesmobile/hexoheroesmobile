using System.Collections.Generic;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.Battle
{
    public class BattlePlayerAvatarsManager : MonoBehaviour
    {
        [SerializeField] private GameObject _playerAvatarsRoot;
        [SerializeField] private GameObject _playerAvatarsTransferRoot;

        public List<PlayerBattleAvatar> Avatars { get; private set; }
        
        public void SetAvatars(List<PlayerBattleAvatar> avatars)
        {
            Avatars = avatars;
            avatars.ForEach(avatar => avatar.transform.parent = _playerAvatarsRoot.transform);
            avatars.ForEach(avatar => avatar.gameObject.SetActive(true));
        }
        
        public void UnloadAvatars()
        {
            // TODO
            var newAvatars = new List<PlayerBattleAvatar>();
            foreach (var avatar in Avatars)
            {
                if (avatar != null)
                {
                    avatar.gameObject.transform.parent = _playerAvatarsTransferRoot.transform;
                    avatar.gameObject.SetActive(false);
                    newAvatars.Add(avatar);
                }
                else
                {
                    // GameObjectUtils.Instantiate(_emptyAvatarPrefab, _avatarsGroup.transform);
                }
            }

            Avatars = newAvatars;
        }
    }
}
