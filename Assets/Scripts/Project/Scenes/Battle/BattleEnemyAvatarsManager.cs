using System.Collections.Generic;
using Project.Avatars.Enemies;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.Battle
{
    public class BattleEnemyAvatarsManager : MonoBehaviour
    {
        [SerializeField] private GameObject _enemyAvatarsRoot;
        
        public List<EnemyBattleAvatar> Avatars { get; private set; } 
        
        public void SetAvatars(List<EnemyBattleAvatar> avatars)
        {
            Avatars = avatars;
            avatars.ForEach(avatar => avatar.transform.parent = _enemyAvatarsRoot.transform);
            avatars.ForEach(avatar => avatar.gameObject.SetActive(true));
        }
    }
}
