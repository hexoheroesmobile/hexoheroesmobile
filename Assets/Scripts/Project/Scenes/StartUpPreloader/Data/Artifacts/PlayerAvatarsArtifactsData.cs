using System.Collections.Generic;
using Unity.Plastic.Newtonsoft.Json;

namespace Project.Scenes.StartUpPreloader.Data.Artifacts
{
    public class PlayerAvatarsArtifactsData
    {
        [JsonProperty("avatars")]
        public List<PlayerAvatarArtifactsData> Avatars { get; set; }
    }
}