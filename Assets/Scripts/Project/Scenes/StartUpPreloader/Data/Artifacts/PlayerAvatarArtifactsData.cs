using System.Collections.Generic;
using Unity.Plastic.Newtonsoft.Json;

namespace Project.Scenes.StartUpPreloader.Data.Artifacts
{
    public class PlayerAvatarArtifactsData
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        
        [JsonProperty("artifacts")]
        public List<PlayerAvatarArtifactData> Artifacts { get; set; }
    }
}