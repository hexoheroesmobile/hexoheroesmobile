using Unity.Plastic.Newtonsoft.Json;

namespace Project.Scenes.StartUpPreloader.Data.Artifacts
{
    public class PlayerAvatarArtifactData
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
