using System.Collections.Generic;
using Unity.Plastic.Newtonsoft.Json;

namespace Project.Scenes.StartUpPreloader.Data.Appearance
{
    public class PlayerAvatarsAppearanceData
    {
        [JsonProperty("avatars")] 
        public List<PlayerAvatarAppearanceData> Avatars { get; set; }
    }
}
