using Project.Avatars.Player;
using Unity.Plastic.Newtonsoft.Json;

namespace Project.Scenes.StartUpPreloader.Data.Appearance
{
    public class PlayerAvatarAppearanceData
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("avatar")]
        public PlayerAvatarsAppearanceCodes Avatar { get; set; } 
    }
}
