using System.Collections.Generic;
using Unity.Plastic.Newtonsoft.Json;

namespace Project.Scenes.StartUpPreloader.Data.Specifications
{
    public class PlayerAvatarsSpecificationsData
    {
        [JsonProperty("avatars")]
        public List<PlayerAvatarSpecificationsData> Avatars { get; set; }
    }
}