using Unity.Plastic.Newtonsoft.Json;

namespace Project.Scenes.StartUpPreloader.Data.Specifications.Types
{
    public class IntSpecificationType
    {
        [JsonProperty("value")]
        public int Value { get; set; }
    }
}