using Project.Scenes.StartUpPreloader.Data.Specifications.Types;
using Unity.Plastic.Newtonsoft.Json;

namespace Project.Scenes.StartUpPreloader.Data.Specifications
{
    public class PlayerAvatarSpecificationsData
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        
        [JsonProperty("level")]
        public IntSpecificationType Level { get; set; }
        
        [JsonProperty("exp")]
        public IntSpecificationType Exp { get; set; }
    }
}