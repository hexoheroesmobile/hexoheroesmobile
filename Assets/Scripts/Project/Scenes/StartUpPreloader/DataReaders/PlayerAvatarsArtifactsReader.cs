using Project.Scenes.StartUpPreloader.Data.Artifacts;
using Project.Scenes.StartUpPreloader.DataFiles;

namespace Project.Scenes.StartUpPreloader.DataReaders
{
    public class PlayerAvatarsArtifactsReader
    {
        public PlayerAvatarsArtifactsData Read()
        {
            return new PlayerAvatarsArtifactsFile().Read();
        }
    }
}