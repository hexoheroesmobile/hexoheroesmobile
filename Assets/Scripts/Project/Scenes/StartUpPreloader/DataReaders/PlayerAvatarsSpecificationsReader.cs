using Project.Scenes.StartUpPreloader.Data.Specifications;
using Project.Scenes.StartUpPreloader.DataFiles;

namespace Project.Scenes.StartUpPreloader.DataReaders
{
    public class PlayerAvatarsSpecificationsReader
    {
        public PlayerAvatarsSpecificationsData Read()
        {
            return new PlayerAvatarsSpecificationsFile().Read();
        }
    }
}