using Project.Scenes.StartUpPreloader.Data.Appearance;
using Project.Scenes.StartUpPreloader.DataFiles;

namespace Project.Scenes.StartUpPreloader.DataReaders
{
    public class PlayerAvatarsAppearanceReader
    {
        public PlayerAvatarsAppearanceData Read()
        {
            return new PlayerAvatarsAppearanceFile().Read();
        }
    }
}
