using System.Collections.Generic;
using System.Linq;
using Common.Scenes;
using Project.Avatars;
using Project.Avatars.Artifacts;
using Project.Avatars.Player;
using Project.Avatars.Specifications;
using Project.Scenes.StartUpPreloader.Creators;
using Project.Scenes.StartUpPreloader.DataReaders;
using Project.Scenes.Transfers;
using UnityEngine;

namespace Project.Scenes.StartUpPreloader
{
    public class StartUpPreloaderSceneManager : ProjectScenePreloaderManager
    {
        [SerializeField] private PlayerHubAvatarsCreator _playerHubAvatarsCreator;
        [SerializeField] private PlayerHubAvatarsArtifactsCreator _playerHubAvatarsArtifactsCreator;
        [SerializeField] private ToHubTransfer _startUpPreloaderToHubTransfer;

        protected override void PreloaderAction()
        {
            var playerHubAvatars = CreatePlayerHubAvatars();
            var allSpecifications = AddPlayerAvatarsSpecifications(playerHubAvatars);
            var allArtifacts = AddPlayerAvatarsArtifacts(playerHubAvatars);

            var avatars = new List<PlayerHubAvatar>();
            foreach (var avatar in playerHubAvatars)
            {
                var artifacts = allArtifacts[avatar.Key];
                var specifications = allSpecifications[avatar.Key];
                var avatarData = new AvatarData();
                avatarData.SetArtifacts(artifacts);
                avatarData.SetSpecifications(specifications);
                
                avatar.Value.SetAvatarData(avatarData);
                avatars.Add(avatar.Value);
            }

            _startUpPreloaderToHubTransfer.Avatars = avatars;

            IsFullyLoaded = true;
        }
        
        private Dictionary<int, PlayerHubAvatar> CreatePlayerHubAvatars()
        {
            var appearanceData = new PlayerAvatarsAppearanceReader().Read();
            var playerHubAvatars = new Dictionary<int, PlayerHubAvatar>();

            foreach (var avatar in appearanceData.Avatars)
            {
                var playerHubAvatar = _playerHubAvatarsCreator.Create(avatar.Avatar);
                playerHubAvatars.Add(avatar.Code, playerHubAvatar);
            }
            
            return playerHubAvatars;
        }

        private Dictionary<int, AvatarSpecifications> AddPlayerAvatarsSpecifications(Dictionary<int, PlayerHubAvatar> playerHubAvatars)
        {
            var avatarsSpecificationsData = new PlayerAvatarsSpecificationsReader().Read();
            var allSpecifications = new Dictionary<int, AvatarSpecifications>();
            foreach (var playerHubAvatar in playerHubAvatars)
            {
                var avatarSpecificationData = avatarsSpecificationsData.Avatars.Find(data => data.Code.Equals(playerHubAvatar.Key));
                var specifications = new PlayerHubAvatarsSpecificationsCreator().Create(avatarSpecificationData);
                allSpecifications.Add(avatarSpecificationData.Code, specifications);
            }

            return allSpecifications;
        }
        
        private Dictionary<int, List<Artifact>> AddPlayerAvatarsArtifacts(Dictionary<int, PlayerHubAvatar> playerHubAvatars)
        {
            var artifactsData = new PlayerAvatarsArtifactsReader().Read();
            var allArtifacts = new Dictionary<int, List<Artifact>>();
            foreach (var playerHubAvatar in playerHubAvatars)
            {
                // TODO
                // var avatar = artifactsData.Avatars.Find(data => data.Code.Equals(playerHubAvatar.Key));
                // foreach (var artifactData in avatar.Artifacts)
                // {
                //     var artifact = _playerHubAvatarsArtifactsCreator.Create(artifactData);
                //     allArtifacts.AddToListValue(avatar.Code, artifact);
                // }
                allArtifacts.Add(playerHubAvatar.Key, new List<Artifact>());
            }

            return allArtifacts;
        }
    }
}
