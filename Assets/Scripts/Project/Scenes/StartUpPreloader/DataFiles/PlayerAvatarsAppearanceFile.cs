using System.Collections.Generic;
using Common.BinaryFormatterUtils;
using Project.Avatars.Player;
using Project.Scenes.StartUpPreloader.Data.Appearance;

namespace Project.Scenes.StartUpPreloader.DataFiles
{
    public class PlayerAvatarsAppearanceFile : BinaryFile<PlayerAvatarsAppearanceData>
    {
        private const string Filepath = "player_avatars_appearance.dat";
    
        public PlayerAvatarsAppearanceFile() : base(Filepath)
        {
        }

        public PlayerAvatarsAppearanceData Read()
        {
            return ReadObject();
        }

        public void Write(PlayerAvatarsAppearanceData data)
        {
            WriteObject(data);
        }

        protected override PlayerAvatarsAppearanceData Default()
        {
            var data = new PlayerAvatarsAppearanceData();
            
            var avatar1 = new PlayerAvatarAppearanceData
            {
                Code = 1,
                Avatar = PlayerAvatarsAppearanceCodes.Avatar1
            };

            var avatar2 = new PlayerAvatarAppearanceData
            {
                Code = 2,
                Avatar = PlayerAvatarsAppearanceCodes.Avatar2
            };

            data.Avatars = new List<PlayerAvatarAppearanceData>
            {
                avatar1,
                avatar2
            };
            
            return data;
        }
    }
}
