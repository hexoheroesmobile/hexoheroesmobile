using System.Collections.Generic;
using Common.BinaryFormatterUtils;
using Project.Scenes.StartUpPreloader.Data.Specifications;
using Project.Scenes.StartUpPreloader.Data.Specifications.Types;

namespace Project.Scenes.StartUpPreloader.DataFiles
{
    public class PlayerAvatarsSpecificationsFile : BinaryFile<PlayerAvatarsSpecificationsData>
    {
        private const string Filepath = "player_avatars_specifications.dat";
    
        public PlayerAvatarsSpecificationsFile() : base(Filepath)
        {
        }

        public PlayerAvatarsSpecificationsData Read()
        {
            return ReadObject();
        }

        public void Write(PlayerAvatarsSpecificationsData data)
        {
            WriteObject(data);
        }

        protected override PlayerAvatarsSpecificationsData Default()
        {
            var data = new PlayerAvatarsSpecificationsData();
            
            var avatar1 = new PlayerAvatarSpecificationsData
            {
                Code = 1,
                Level = new IntSpecificationType
                {
                    Value = 1
                },
                Exp = new IntSpecificationType
                {
                    Value = 0
                }
            };

            var avatar2 = new PlayerAvatarSpecificationsData
            {
                Code = 2,
                Level = new IntSpecificationType
                {
                    Value = 1
                },
                Exp = new IntSpecificationType
                {
                    Value = 0
                }
            };

            data.Avatars = new List<PlayerAvatarSpecificationsData>
            {
                avatar1,
                avatar2
            };

            return data;
        }
    }
}