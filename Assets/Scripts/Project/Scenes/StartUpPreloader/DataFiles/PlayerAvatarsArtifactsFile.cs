using System.Collections.Generic;
using Common.BinaryFormatterUtils;
using Project.Scenes.StartUpPreloader.Data.Artifacts;

namespace Project.Scenes.StartUpPreloader.DataFiles
{
    public class PlayerAvatarsArtifactsFile : BinaryFile<PlayerAvatarsArtifactsData>
    {
        private const string Filepath = "player_avatars_artifacts.dat";
    
        public PlayerAvatarsArtifactsFile() : base(Filepath)
        {
        }

        public PlayerAvatarsArtifactsData Read()
        {
            return ReadObject();
        }

        public void Write(PlayerAvatarsArtifactsData data)
        {
            WriteObject(data);
        }

        protected override PlayerAvatarsArtifactsData Default()
        {
            var data = new PlayerAvatarsArtifactsData();
            
            var avatar1 = new PlayerAvatarArtifactsData
            {
                Code = 1,
                Artifacts = new List<PlayerAvatarArtifactData>()
            };

            var avatar2 = new PlayerAvatarArtifactsData
            {
                Code = 2,
                Artifacts = new List<PlayerAvatarArtifactData>()
            };

            data.Avatars = new List<PlayerAvatarArtifactsData>
            {
                avatar1,
                avatar2
            };
            
            return data;
        }
    }
}