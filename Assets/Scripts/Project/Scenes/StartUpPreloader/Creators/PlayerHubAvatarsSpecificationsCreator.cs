using Project.Avatars.Specifications;
using Project.Avatars.Specifications.Damage;
using Project.Avatars.Specifications.Health;
using Project.Avatars.Specifications.Initiative;
using Project.Avatars.Specifications.MovesCount;
using Project.Scenes.StartUpPreloader.Data.Specifications;

namespace Project.Scenes.StartUpPreloader.Creators
{
    public class PlayerHubAvatarsSpecificationsCreator
    {
        public AvatarSpecifications Create(PlayerAvatarSpecificationsData data)
        {
            var specifications = new AvatarSpecifications();
            
            // TODO Add logic
            specifications.SetInitiative(new InitiativeSpecification(5, 5));
            specifications.SetHealth(new HealthSpecification(10, 10));
            specifications.SetDamage(new DamageSpecification(1, 3));
            specifications.SetMovesCount(new MovesCountSpecification(1, 1));
            
            return specifications;
        }
    }
}