using System.Collections.Generic;
using Common.Utils;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.StartUpPreloader.Creators
{
    public class PlayerHubAvatarsCreator : MonoBehaviour
    {
        [SerializeField] private List<PlayerHubAvatar> _playerHubAvatars;
        [SerializeField] private GameObject _playerHubAvatarsRoot;

        public PlayerHubAvatar Create(PlayerAvatarsAppearanceCodes avatarCode)
        {
            var prefabAvatar = _playerHubAvatars.Find(avatar => avatar.name.Equals(avatarCode.ToString()));
            var playerHubAvatar = GameObjectUtils.InstantiateNotActive(prefabAvatar, _playerHubAvatarsRoot.transform, true);
            return playerHubAvatar;
        }
    }
}
