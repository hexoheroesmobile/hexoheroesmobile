using Project.Avatars.Artifacts;
using Project.Scenes.StartUpPreloader.Data.Artifacts;
using UnityEngine;

namespace Project.Scenes.StartUpPreloader.Creators
{
    public class PlayerHubAvatarsArtifactsCreator : MonoBehaviour
    {
        public Artifact Create(PlayerAvatarArtifactData data)
        {
            return new Artifact();
        }
    }
}