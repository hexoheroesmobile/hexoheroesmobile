using System.Collections.Generic;
using Common.Utils;
using Project.Avatars.Player;
using UnityEngine;
using UnityEngine.Events;

namespace Project.Scenes.Hub
{
    public class HubAvatarsManager : MonoBehaviour
    {
        public UnityEvent<AvatarEventArgs> AvatarAdded { get; } = new UnityEvent<AvatarEventArgs>();
        public UnityEvent<AvatarEventArgs> AvatarRemoved { get; } = new UnityEvent<AvatarEventArgs>();

        private List<PlayerHubAvatar> _avatars = new List<PlayerHubAvatar>();

        public void SetAvatars(List<PlayerHubAvatar> avatars)
        {
            for (var i = 0; i < avatars.Count; i++)
            {
                _avatars.Add(avatars[i]);
                Locate(avatars[i].gameObject, i);
                AvatarAdded.Invoke(GetAvatarEventArgs(avatars[i]));
            }
        }

        public void AddAvatar(PlayerHubAvatar avatar)
        {
            _avatars.Add(avatar);
            AvatarAdded.Invoke(GetAvatarEventArgs(avatar));
        }

        public void RemoveAvatar(PlayerHubAvatar avatar)
        {
            _avatars.Remove(avatar);
            AvatarRemoved.Invoke(GetAvatarEventArgs(avatar));
        }
        
        private void Locate(GameObject avatar, int index)
        {
            // TODO
            avatar.transform.SetXPosition(2.0f * index);
            //
        }

        private AvatarEventArgs GetAvatarEventArgs(PlayerHubAvatar avatar)
        {
            return new AvatarEventArgs { Avatar = avatar};
        }
    }
    
    public class AvatarEventArgs
    {
        public PlayerHubAvatar Avatar { get; set; }
    }
}
