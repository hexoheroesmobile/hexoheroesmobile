using System.Collections.Generic;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.Hub
{
    public class HubAvatarsGroupManager : MonoBehaviour
    {
        public List<PlayerHubAvatar> Avatars { get; private set; } = new List<PlayerHubAvatar> { null, null, null };

        [SerializeField] private GameObject _avatarsGroup;
        [SerializeField] private GameObject _emptyAvatarPrefab;

        public void SetAvatarToSlot(int slot, PlayerHubAvatar avatar)
        {
            Avatars[slot] = avatar;
        }

        public void RemoveAvatarFromSlot(int slot)
        {
            Avatars[slot] = null;
        }

        public void UnloadAvatars()
        {
            // TODO
            var newAvatars = new List<PlayerHubAvatar>();
            for (var i = 0; i < 3; i++)
            {
                if (Avatars[i] != null)
                {
                    Avatars[i].gameObject.transform.parent = _avatarsGroup.transform;
                    Avatars[i].gameObject.SetActive(false);
                    newAvatars.Add(Avatars[i]);
                }
                else
                {
                    // GameObjectUtils.Instantiate(_emptyAvatarPrefab, _avatarsGroup.transform);
                }
            }

            Avatars = newAvatars;
        }
    }
}
