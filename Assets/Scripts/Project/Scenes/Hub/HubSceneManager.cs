using Common.Scenes;
using Project.Scenes.Transfers;
using UnityEditor;
using UnityEngine;

namespace Project.Scenes.Hub
{
    public class HubSceneManager : ProjectSceneManager
    {
        [SerializeField] private SceneAsset _battlePreloaderScene;
        [SerializeField] private HubAvatarsManager _avatarsManager;
        [SerializeField] private HubAvatarsGroupManager _avatarsGroupManager;
        [SerializeField] private HubToBattlePreloaderTransfer _hubToBattlePreloaderTransfer;
        [SerializeField] private GameObject _hubAvatarsRoot;

        private void Start()
        {
            var prevSceneTransfer = FindObjectOfType<ToHubTransfer>();
            var avatars = prevSceneTransfer.Avatars;

            avatars.ForEach(avatar => avatar.transform.parent = _hubAvatarsRoot.transform);
            avatars.ForEach(avatar => avatar.gameObject.SetActive(true));
            
            _avatarsManager.SetAvatars(avatars);
            
            // TODO
            for (var i = 0; i < avatars.Count; i++)
            {
                _avatarsGroupManager.SetAvatarToSlot(i, avatars[i]);
            }
            //

            IsFullyLoaded = true;
        }
        
        public void ToBattle()
        {
            StartCoroutine(LoadNextSceneWithBeforeStep(PrepareAvatars, _battlePreloaderScene));
        }

        private void PrepareAvatars()
        {
            // _avatarsGroupManager.UnloadAvatars();
            // _hubToBattlePreloaderTransfer.PlayerHubAvatars = _avatarsGroupManager.Avatars;
            _avatarsGroupManager.UnloadAvatars();
            _hubToBattlePreloaderTransfer.PlayerHubAvatars = _avatarsGroupManager.Avatars;
        }
    }
}
