using System.Collections.Generic;
using System.Linq;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.BattlePreloader
{
    public class BattlePreloaderPlayerBattleAvatarsCreator : MonoBehaviour
    {
        [SerializeField] private GameObject _playerBattleAvatars;
        public List<PlayerBattleAvatar> CreatePlayerBattleAvatars(List<PlayerHubAvatar> playerHubAvatars)
        {
            var playerHubAvatarsData = playerHubAvatars.ToDictionary(avatar => avatar.gameObject, avatar => avatar.AvatarData);
            var playerBattleAvatars = new List<PlayerBattleAvatar>();
            foreach (var playerHubAvatarData in playerHubAvatarsData)
            {
                Destroy(playerHubAvatarData.Key.GetComponent<PlayerHubAvatar>());
                playerHubAvatarData.Key.AddComponent<PlayerBattleAvatar>();
                var playerBattleAvatar = playerHubAvatarData.Key.GetComponent<PlayerBattleAvatar>();
                playerBattleAvatar.SetAvatarData(playerHubAvatarData.Value);
                playerBattleAvatar.transform.parent = _playerBattleAvatars.transform;
                playerBattleAvatars.Add(playerBattleAvatar);
            }

            return playerBattleAvatars;
        }
    }
}
