using System.Collections.Generic;
using Common.Utils;
using Project.Avatars.Enemies;
using UnityEngine;

namespace Project.Scenes.BattlePreloader
{
    public class BattlePreloaderEnemyBattleAvatarsCreator : MonoBehaviour
    {
        [SerializeField] private List<EnemyBattleAvatarTemplate> _enemyBattleAvatarTemplates;
        [SerializeField] private GameObject _enemyBattleAvatarsRoot;
        
        public List<EnemyBattleAvatar> Create(int power)
        {
            // TODO
            var prefabAvatarTemplate = _enemyBattleAvatarTemplates.Find(avatar => avatar.GetPower() == power);
            var enemyHubAvatar1 = prefabAvatarTemplate.Create();
            var enemyHubAvatar2 = prefabAvatarTemplate.Create();
            enemyHubAvatar1.transform.SetParent(_enemyBattleAvatarsRoot.transform);
            enemyHubAvatar2.transform.SetParent(_enemyBattleAvatarsRoot.transform);
            
            return new List<EnemyBattleAvatar> { enemyHubAvatar1, enemyHubAvatar2 };
        }
    }
}
