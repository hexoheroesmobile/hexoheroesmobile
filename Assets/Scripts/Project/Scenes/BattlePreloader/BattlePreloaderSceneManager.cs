using System.Collections.Generic;
using Common.Scenes;
using Project.Avatars.Enemies;
using Project.Scenes.Transfers;
using UnityEngine;

namespace Project.Scenes.BattlePreloader
{
    public class BattlePreloaderSceneManager : ProjectScenePreloaderManager
    {
        [SerializeField] private BattlePreloaderEnemyBattleAvatarsCreator _battlePreloaderEnemyBattleAvatarsCreator;
        [SerializeField] private BattlePreloaderPlayerBattleAvatarsCreator _battlePreloaderPlayerBattleAvatarsCreator;
        [SerializeField] private BattlePreloaderAvatarsLocation _battlePreloaderAvatarsLocation;
        [SerializeField] private BattlePreloaderToBattleTransfer _battlePreloaderToBattleTransfer;

        protected override void PreloaderAction()
        {
            var prevSceneTransfer = FindObjectOfType<HubToBattlePreloaderTransfer>();
            var playerHubAvatars = prevSceneTransfer.PlayerHubAvatars;
            var playerBattleAvatars = _battlePreloaderPlayerBattleAvatarsCreator.CreatePlayerBattleAvatars(playerHubAvatars);
            _battlePreloaderAvatarsLocation.LocatePlayerAvatars(playerBattleAvatars);
            
            var power = new BattlePreloaderEnemiesPowerCalculator().CalculatePower(playerBattleAvatars);
            var enemyBattleAvatars = _battlePreloaderEnemyBattleAvatarsCreator.Create(power);
            _battlePreloaderAvatarsLocation.LocateEnemyAvatars(enemyBattleAvatars);

            _battlePreloaderToBattleTransfer.PlayerBattleAvatars = playerBattleAvatars;
            _battlePreloaderToBattleTransfer.EnemyBattleAvatars = enemyBattleAvatars;
        }
    }
}
