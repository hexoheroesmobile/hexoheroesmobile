using System.Collections.Generic;
using Project.Avatars.Enemies;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.BattlePreloader
{
    public class BattlePreloaderAvatarsLocation : MonoBehaviour
    {
        [SerializeField] private List<GameObject> _playerAvatarSpots;
        [SerializeField] private List<GameObject> _enemyAvatarSpots;

        public void LocatePlayerAvatars(List<PlayerBattleAvatar> avatars)
        {
            for (var i = 0; i < avatars.Count; i++)
            {
                avatars[i].transform.position = _playerAvatarSpots[i].transform.position;
            }
        }
        
        public void LocateEnemyAvatars(List<EnemyBattleAvatar> avatars)
        {
            for (var i = 0; i < avatars.Count; i++)
            {
                avatars[i].transform.position = _enemyAvatarSpots[i].transform.position;
            }
        }
    }
}
