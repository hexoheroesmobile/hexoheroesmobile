using System.Collections.Generic;
using Common.Scenes;
using Project.Avatars.Player;
using Project.Scenes.Transfers;
using UnityEngine;

namespace Project.Scenes.AfterBattlePreloader
{
    public class AfterBattlePreloaderSceneManager : ProjectScenePreloaderManager
    {
        [SerializeField] private AfterBattlePreloaderHubAvatarsCreator _afterBattlePreloaderHubAvatarsCreator;
        [SerializeField] private ToHubTransfer _afterBattlePreloaderToHubTransfer;
        
        protected override void PreloaderAction()
        {
            var prevSceneTransfer = FindObjectOfType<BattleToAfterBattlePreloaderTransfer>();
            var playerBattleAvatars = prevSceneTransfer.Avatars;

            var hubAvatars = _afterBattlePreloaderHubAvatarsCreator.CreatePlayerHubAvatars(playerBattleAvatars);
            
            _afterBattlePreloaderToHubTransfer.Avatars = hubAvatars;
            
            IsFullyLoaded = true;
        }
    }
}
