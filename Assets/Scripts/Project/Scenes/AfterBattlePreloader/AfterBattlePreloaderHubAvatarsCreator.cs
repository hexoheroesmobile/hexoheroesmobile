using System.Collections.Generic;
using System.Linq;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.AfterBattlePreloader
{
    public class AfterBattlePreloaderHubAvatarsCreator : MonoBehaviour
    {
        [SerializeField] private GameObject _playerHubAvatars;
        
        public List<PlayerHubAvatar> CreatePlayerHubAvatars(List<PlayerBattleAvatar> playerBattleAvatars)
        {
            var playerBattleAvatarsData = playerBattleAvatars.ToDictionary(avatar => avatar.gameObject, avatar => avatar.AvatarData);
            var playerHubAvatars = new List<PlayerHubAvatar>();
            foreach (var playerBattleAvatarData in playerBattleAvatarsData)
            {
                Destroy(playerBattleAvatarData.Key.GetComponent<PlayerBattleAvatar>());
                playerBattleAvatarData.Key.AddComponent<PlayerHubAvatar>();
                var playerHubAvatar = playerBattleAvatarData.Key.GetComponent<PlayerHubAvatar>();
                playerHubAvatar.SetAvatarData(playerBattleAvatarData.Value);
                playerHubAvatar.transform.parent = _playerHubAvatars.transform;
                playerHubAvatars.Add(playerHubAvatar);
            }
            
            return playerHubAvatars;
        }
    }
}
