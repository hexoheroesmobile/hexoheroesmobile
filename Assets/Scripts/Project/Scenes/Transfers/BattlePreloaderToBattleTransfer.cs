using System.Collections.Generic;
using Project.Avatars.Enemies;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.Transfers
{
    public class BattlePreloaderToBattleTransfer : MonoBehaviour
    {
        public List<PlayerBattleAvatar> PlayerBattleAvatars { get; set; }
        public List<EnemyBattleAvatar> EnemyBattleAvatars { get; set; }
    }
}
