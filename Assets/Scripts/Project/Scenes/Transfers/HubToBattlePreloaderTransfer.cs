using System.Collections.Generic;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.Transfers
{
    public class HubToBattlePreloaderTransfer : MonoBehaviour
    {
        public List<PlayerHubAvatar> PlayerHubAvatars { get; set; }
    }
}
