using System.Collections.Generic;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.Transfers
{
    public class BattleToAfterBattlePreloaderTransfer : MonoBehaviour
    {
        public List<PlayerBattleAvatar> Avatars { get; set; }
    }
}
