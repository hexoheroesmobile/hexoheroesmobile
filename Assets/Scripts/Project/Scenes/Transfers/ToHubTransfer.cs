using System.Collections.Generic;
using Project.Avatars.Player;
using UnityEngine;

namespace Project.Scenes.Transfers
{
    public class ToHubTransfer : MonoBehaviour
    {
        public List<PlayerHubAvatar> Avatars { get; set; }
    }
}
