using Project.Avatars.Player;
using Project.Scenes.Hub;
using UnityEngine;

namespace Project.UI.Hub
{
    public class HubUIManager : MonoBehaviour
    {
        [SerializeField] private HubAvatarsManager _avatarsManager;
        [SerializeField] private PlayerHubAvatarInfoPanel _playerHubAvatarInfoPanel;

        private void Start()
        {
            _avatarsManager.AvatarAdded.AddListener(AvatarAdded);
        }

        private void AvatarAdded(AvatarEventArgs args)
        {
            args.Avatar.Selected.AddListener(() => AvatarSelected(args.Avatar));
        }

        private void AvatarSelected(PlayerHubAvatar avatar)
        {
            _playerHubAvatarInfoPanel.SetAvatarSpecifications(avatar.AvatarData.Specifications);
        }
    }
}
