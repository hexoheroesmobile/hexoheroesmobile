using Project.Avatars.Specifications;
using TMPro;
using UnityEngine;

namespace Project.UI.Hub
{
    public class PlayerHubAvatarInfoPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _initiativeText;
        [SerializeField] private TextMeshProUGUI _healthText;
        [SerializeField] private TextMeshProUGUI _damageText;

        public void SetAvatarSpecifications(AvatarSpecifications specifications)
        {
            _initiativeText.text = $"{specifications.GetInitiative().GetCurrent()}";
            _healthText.text = $"{specifications.GetHealth().GetCurrent()} / {specifications.GetHealth().GetMax()}";
            _damageText.text = $"{specifications.GetDamage().GetMin()} - {specifications.GetDamage().GetMax()}";
        }
    }
}
