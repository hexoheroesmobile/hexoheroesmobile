using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common;
using Common.Scenes;
using Project.Scenes.Hub;
using UnityEditor;
using Common.Tools.AutoTransitionsWindowTool;
using Common.Utils;
using Project.Scenes.Battle;
using UnityEngine.SceneManagement;

namespace Project.Tools.AutoSceneTransitionsTool.Editor
{
    public class AutoSceneTransitions
    {
        private const string StartUpPreloaderSceneName = "StartUpPreloader";
        private const string HubSceneName = "Hub";
        private const string BattlePreloaderSceneName = "BattlePreloader";
        private const string BattleSceneName = "Battle";
        private const string AfterBattlePreloaderSceneName = "AfterBattlePreloader";

        private readonly List<SceneTransition> _transitions;

        public AutoSceneTransitions()
        {
            var scenePaths = AssetUtils.GetBuiltScenePaths();
            var sceneNames = scenePaths.Select(Path.GetFileNameWithoutExtension).ToList();
            
            var startUpPreloaderScenePath = scenePaths[sceneNames.IndexOf(StartUpPreloaderSceneName)];
            var hubScenePath = scenePaths[sceneNames.IndexOf(HubSceneName)];
            var battlePreloaderScenePath = scenePaths[sceneNames.IndexOf(BattlePreloaderSceneName)];
            var battleScenePath = scenePaths[sceneNames.IndexOf(BattleSceneName)];
            var afterBattlePreloaderScenePath = scenePaths[sceneNames.IndexOf(AfterBattlePreloaderSceneName)];
            
            var startUpPreloaderScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(startUpPreloaderScenePath);
            var hubScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(hubScenePath);
            var battlePreloaderScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(battlePreloaderScenePath);
            var battleScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(battleScenePath);
            var afterBattlePreloaderScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(afterBattlePreloaderScenePath);

            _transitions = new List<SceneTransition>();

            var sceneTransition = new SceneTransition(startUpPreloaderScene, hubScene, Nothing);
            _transitions.Add(sceneTransition);

            sceneTransition = new SceneTransition(hubScene, battlePreloaderScene, ToBattle);
            _transitions.Add(sceneTransition);
            
            sceneTransition = new SceneTransition(battlePreloaderScene, battleScene, Nothing);
            _transitions.Add(sceneTransition);

            sceneTransition = new SceneTransition(battleScene, afterBattlePreloaderScene, ToHub);
            _transitions.Add(sceneTransition);

            sceneTransition = new SceneTransition(afterBattlePreloaderScene, hubScene, Nothing);
            _transitions.Add(sceneTransition);
        }
        
        public List<SceneTransition> GetSceneTransitions()
        {
            return _transitions;
        }

        private static bool Nothing()
        {
            return true;
        }

        private static bool ToBattle()
        {
            var sceneManager = GetActiveLoadedSceneManager();
            if (sceneManager == null) return false;
            ((HubSceneManager)sceneManager).ToBattle();
            return true;
        }
        
        private static bool ToHub()
        {
            var sceneManager = GetActiveLoadedSceneManager();
            if (sceneManager == null) return false;
            ((BattleSceneManager)sceneManager).Finish();
            return true;
        }

        private static ProjectSceneManager GetActiveLoadedSceneManager()
        {
            var allObject = SceneManager.GetActiveScene().GetRootGameObjects().ToList();
            foreach (var obj in allObject)
            {
                var sceneManagers = obj.GetComponentsInChildren<ProjectSceneManager>(true);
                if (sceneManagers.Length == 0) continue;
                return sceneManagers[0].IsFullyLoaded ? sceneManagers[0] : null;
            }

            return null;
        }
    }
}
