using System.Collections.Generic;
using Common.Tools.AutoTransitionsWindowTool;
using Common.UI.Toolbar;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Project.Tools.AutoSceneTransitionsTool.Editor
{
    public class SceneTransitionsDropdown : Element
    {
        public UnityEvent<List<int>> Selected { get; } = new UnityEvent<List<int>>();
        
        private readonly float _width;
        private Rect _buttonRect;
        private readonly List<SceneTransition> _transitions;
        private AutoSceneTransitionWindow _targetWindow;

        public SceneTransitionsDropdown(float width, List<SceneTransition> transitions)
        {
            _width = width;
            _transitions = transitions;
        }

        protected override void Repaint()
        {
            if (EditorGUILayout.DropdownButton(new GUIContent("Transitions"), FocusType.Passive, UnityEngine.GUI.skin.FindStyle("toolbarPopup"), GUILayout.Width(_width), GUILayout.ExpandHeight(true)))
            {
                _targetWindow = (AutoSceneTransitionWindow)ScriptableObject.CreateInstance(typeof(AutoSceneTransitionWindow));
                _targetWindow.SetTransitions(_transitions);
                _targetWindow.Init();
                _targetWindow.ShowAsDropDown(_buttonRect, new Vector2(220, 300));
            }

            if (_targetWindow != null)
            {
                var result = _targetWindow.GetSelectedTransitionIndexes();
                base.Changed();
                Selected.Invoke(result);
            }
            
            if (Event.current.type == EventType.Repaint) _buttonRect = GUIUtility.GUIToScreenRect(GUILayoutUtility.GetLastRect());
        }
    }
}