using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common;
using Common.Tools.AutoTransitionsWindowTool;
using Common.Tools.ToolbarExtender;
using Common.UI.Toolbar.Elements;
using Common.Utils;
using Unity.Plastic.Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Project.Tools.AutoSceneTransitionsTool.Editor
{
    [InitializeOnLoad]
    public static class AutoSceneTransitionsToolbar
    {
        static AutoSceneTransitionsToolbar()
        {
            var toolbar = new AutoSceneTransitionsToolbarImpl();
            ToolbarExtender.AddLeftBlock(toolbar, 0);
            EditorApplication.update += toolbar.Update;
        }
    }

    internal class AutoSceneTransitionsToolbarImpl : ToolbarBlock
    {
        private int _currentTransitionCursor;
        private string _currentScenePath;
        private SceneTransition _targetTransition;
        private readonly AutoSceneTransitions _sceneTransitions;
        private List<int> _selectedTransitionIndexes;
        private readonly SceneTransitionsDropdown _dropdown;
        
        public AutoSceneTransitionsToolbarImpl()
        {
            _currentTransitionCursor = 0;
            _sceneTransitions = new AutoSceneTransitions();
            
            SceneManager.activeSceneChanged += ActiveSceneChanged;
            
            _dropdown = new SceneTransitionsDropdown(150, _sceneTransitions.GetSceneTransitions());
            _dropdown.Selected.AddListener(GetSelectedIndexes);
            Elements.Add(_dropdown);

            var icon = (Texture)EditorGUIUtility.Load("auto_transitions_arrow.png");
            var button = new IconButton(icon, 30);
            button.Clicked.AddListener(LoadScene);
            Elements.Add(button);
            
            LoadState();
        }

        private void GetSelectedIndexes(List<int> indexes)
        {
            _selectedTransitionIndexes = indexes;
            SaveState();
        }

        private void LoadScene()
        {
            if (EditorApplication.isPlaying)
            {
                EditorApplication.ExitPlaymode();
                EditorSceneManager.playModeStartScene = null;
                SceneManager.LoadScene(_currentScenePath);
            }
            else
            {
                SaveState();
                _currentScenePath = SceneManager.GetActiveScene().path;
                var scenePaths = AssetUtils.GetBuiltScenePaths();
                EditorSceneManager.playModeStartScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePaths[0]);
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                {
                    EditorApplication.EnterPlaymode();
                }
            }
        }
        
        public void Update()
        {
            try
            {
                if (_targetTransition != null && _targetTransition.Action())
                {
                    _targetTransition = null;
                }
                _dropdown.SetActive(Application.isPlaying == false);
            }
            catch (Exception e)
            {
                // ignored
            }
        }
        
        private void ActiveSceneChanged(Scene arg0, Scene arg1)
        {
            if (Application.isPlaying == false) return;
            if (_currentTransitionCursor >= _selectedTransitionIndexes.Count) return;
            var transition = _sceneTransitions.GetSceneTransitions()[_selectedTransitionIndexes[_currentTransitionCursor]];
            _targetTransition = transition;
            _currentTransitionCursor += 1;
        }
        
        protected override void SaveWindowState(JArray array)
        {
            var arr = JArray.FromObject(_selectedTransitionIndexes);
            array.Add(arr);
            base.SaveWindowState(array);
        }
        
        protected override void LoadWindowState(JArray array)
        {
            if (array.Count > 0)
            {
                try
                {
                    _selectedTransitionIndexes = array[0].ToObject<List<int>>();
                    array.RemoveAt(0);
                }
                catch (Exception e)
                {
                    // ignored
                }
            }
            
            base.LoadWindowState(array);
        }
    }
}
